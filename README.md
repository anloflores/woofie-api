# Woofie API

This project provides a comprehensive API for managing users, pets, posts, and comments. It includes authentication, user management, pet management, post creation and interaction, and comment management.

## Requirements

- PHP 7.4 or higher
- Laravel 8.x or higher
- Composer
- MySQL or any other supported database

## Installation

1. Clone the repository:
   ```bash
   git clone https://github.com/your-repo/project-name.git
   cd project-name
   ```

2. Install dependencies:
   ```bash
   composer install
   ```

3. Copy `.env.example` to `.env` and configure your environment variables:
   ```bash
   cp .env.example .env
   ```

4. Generate the application key:
   ```bash
   php artisan key:generate
   ```

5. Run migrations and seeders:
   ```bash
   php artisan migrate --seed
   ```

6. Serve the application:
   ```bash
   php artisan serve
   ```

## Usage

### Authentication

- **Register:** `POST /api/v1/register`
- **Login:** `POST /api/v1/login`
- **Logout:** `POST /api/v1/logout` (Requires authentication)

### Users

- **Get all users:** `GET /api/v1/users` (Requires authentication)
- **Create a new user:** `POST /api/v1/users` (Requires authentication)
- **Get a specific user:** `GET /api/v1/users/{id}` (Requires authentication)
- **Update a user:** `PUT /api/v1/users/{id}` (Requires authentication)
- **Delete a user:** `DELETE /api/v1/users/{id}` (Requires authentication)
- **Follow/Unfollow a user:** `POST /api/v1/users/{userId}/follow` (Requires authentication)

### Pets

- **Get all pets:** `GET /api/v1/pets` (Requires authentication)
- **Create a new pet:** `POST /api/v1/pets` (Requires authentication)
- **Get a specific pet:** `GET /api/v1/pets/{id}` (Requires authentication)
- **Update a pet:** `PUT /api/v1/pets/{id}` (Requires authentication)
- **Delete a pet:** `DELETE /api/v1/pets/{id}` (Requires authentication)
- **Get pets by user ID:** `GET /api/v1/users/{id}/pets` (Requires authentication)
- **Upload pet attachment:** `POST /api/v1/pets/{id}/upload` (Requires authentication)

### Posts

- **Get all posts:** `GET /api/v1/posts` (Requires authentication)
- **Create a new post:** `POST /api/v1/posts` (Requires authentication)
- **Get a specific post:** `GET /api/v1/posts/{id}` (Requires authentication)
- **Update a post:** `PUT /api/v1/posts/{id}` (Requires authentication)
- **Delete a post:** `DELETE /api/v1/posts/{id}` (Requires authentication)
- **Like/Unlike a post:** `POST /api/v1/posts/{id}/like` (Requires authentication)
- **Share a post:** `POST /api/v1/posts/{id}/share` (Requires authentication)
- **Add media to a post:** `POST /api/v1/posts/{id}/media` (Requires authentication)

### Comments

- **Get all comments:** `GET /api/v1/comments` (Requires authentication)
- **Create a new comment:** `POST /api/v1/comments` (Requires authentication)
- **Get a specific comment:** `GET /api/v1/comments/{id}` (Requires authentication)
- **Update a comment:** `PUT /api/v1/comments/{id}` (Requires authentication)
- **Delete a comment:** `DELETE /api/v1/comments/{id}` (Requires authentication)
- **Like/Unlike a comment:** `POST /api/v1/comments/{id}/like` (Requires authentication)


## License

This project is licensed under the MIT License. See the [LICENSE](LICENSE) file for details.

---

### API Documentation

#### Authentication

##### Register
- **URL:** `/api/v1/register`
- **Method:** `POST`
- **Description:** Registers a new user.
- **Body Parameters:**
  - `name` (string, required)
  - `email` (string, required)
  - `password` (string, required)

##### Login
- **URL:** `/api/v1/login`
- **Method:** `POST`
- **Description:** Logs in a user.
- **Body Parameters:**
  - `email` (string, required)
  - `password` (string, required)

##### Logout
- **URL:** `/api/v1/logout`
- **Method:** `POST`
- **Description:** Logs out the authenticated user.
- **Headers:**
  - `Authorization: Bearer {token}`

#### User Routes

##### Get All Users
- **URL:** `/api/v1/users`
- **Method:** `GET`
- **Description:** Retrieves a list of all users.
- **Headers:**
  - `Authorization: Bearer {token}`

##### Create User
- **URL:** `/api/v1/users`
- **Method:** `POST`
- **Description:** Creates a new user.
- **Headers:**
  - `Authorization: Bearer {token}`
- **Body Parameters:**
  - `name` (string, required)
  - `email` (string, required)
  - `password` (string, required)

##### Get Specific User
- **URL:** `/api/v1/users/{id}`
- **Method:** `GET`
- **Description:** Retrieves details of a specific user.
- **Headers:**
  - `Authorization: Bearer {token}`

##### Update User
- **URL:** `/api/v1/users/{id}`
- **Method:** `PUT`
- **Description:** Updates a specific user.
- **Headers:**
  - `Authorization: Bearer {token}`
- **Body Parameters:**
  - `name` (string, optional)
  - `email` (string, optional)
  - `password` (string, optional)

##### Delete User
- **URL:** `/api/v1/users/{id}`
- **Method:** `DELETE`
- **Description:** Deletes a specific user.
- **Headers:**
  - `Authorization: Bearer {token}`

##### Follow/Unfollow User
- **URL:** `/api/v1/users/{userId}/follow`
- **Method:** `POST`
- **Description:** Follows or unfollows a user.
- **Headers:**
  - `Authorization: Bearer {token}`

#### Pet Routes

##### Get All Pets
- **URL:** `/api/v1/pets`
- **Method:** `GET`
- **Description:** Retrieves a list of all pets.
- **Headers:**
  - `Authorization: Bearer {token}`

##### Create Pet
- **URL:** `/api/v1/pets`
- **Method:** `POST`
- **Description:** Creates a new pet.
- **Headers:**
  - `Authorization: Bearer {token}`
- **Body Parameters:**
  - `name` (string, required)
  - `type` (string, required)
  - `age` (integer, required)

##### Get Specific Pet
- **URL:** `/api/v1/pets/{id}`
- **Method:** `GET`
- **Description:** Retrieves details of a specific pet.
- **Headers:**
  - `Authorization: Bearer {token}`

##### Update Pet
- **URL:** `/api/v1/pets/{id}`
- **Method:** `PUT`
- **Description:** Updates a specific pet.
- **Headers:**
  - `Authorization: Bearer {token}`
- **Body Parameters:**
  - `name` (string, optional)
  - `type` (string, optional)
  - `age` (integer, optional)

##### Delete Pet
- **URL:** `/api/v1/pets/{id}`
- **Method:** `DELETE`
- **Description:** Deletes a specific pet.
- **Headers:**
  - `Authorization: Bearer {token}`

##### Get Pets by User ID
- **URL:** `/api/v1/users/{id}/pets`
- **Method:** `GET`
- **Description:** Retrieves pets associated with a specific user.
- **Headers:**
  - `Authorization: Bearer {token}`

##### Upload Pet Attachment
- **URL:** `/api/v1/pets/{id}/upload`
- **Method:** `POST`
- **Description:** Uploads an attachment for a specific pet.
- **Headers:**
  - `Authorization: Bearer {token}`
- **Body Parameters:**
  - `file` (file, required)

#### Post Routes

##### Get All Posts
- **URL:** `/api/v1/posts`
- **Method:** `GET`
- **Description:** Retrieves a list of all posts.
- **Headers:**
  - `Authorization: Bearer {token}`

##### Create Post
- **URL:** `/api/v1/posts`
- **Method:** `POST`
- **Description:** Creates a new post.
- **Headers:**
  - `Authorization: Bearer {token}`
- **Body Parameters:**
  - `content` (string, required)

##### Get Specific Post
URL:** `/api/v1/posts/{id}`
- **Method:** `GET`
- **Description:** Retrieves details of a specific post.
- **Headers:**
  - `Authorization: Bearer {token}`

##### Update Post
- **URL:** `/api/v1/posts/{id}`
- **Method:** `PUT`
- **Description:** Updates a specific post.
- **Headers:**
  - `Authorization: Bearer {token}`
- **Body Parameters:**
  - `content` (string, optional)

##### Delete Post
- **URL:** `/api/v1/posts/{id}`
- **Method:** `DELETE`
- **Description:** Deletes a specific post.
- **Headers:**
  - `Authorization: Bearer {token}`

##### Like/Unlike Post
- **URL:** `/api/v1/posts/{id}/like`
- **Method:** `POST`
- **Description:** Likes or unlikes a post.
- **Headers:**
  - `Authorization: Bearer {token}`

##### Share Post
- **URL:** `/api/v1/posts/{id}/share`
- **Method:** `POST`
- **Description:** Shares a post.
- **Headers:**
  - `Authorization: Bearer {token}`

##### Add Media to Post
- **URL:** `/api/v1/posts/{id}/media`
- **Method:** `POST`
- **Description:** Adds media to a post.
- **Headers:**
  - `Authorization: Bearer {token}`
- **Body Parameters:**
  - `file` (file, required)

#### Comment Routes

##### Get All Comments
- **URL:** `/api/v1/comments`
- **Method:** `GET`
- **Description:** Retrieves a list of all comments.
- **Headers:**
  - `Authorization: Bearer {token}`

##### Create Comment
- **URL:** `/api/v1/comments`
- **Method:** `POST`
- **Description:** Creates a new comment.
- **Headers:**
  - `Authorization: Bearer {token}`
- **Body Parameters:**
  - `content` (string, required)

##### Get Specific Comment
- **URL:** `/api/v1/comments/{id}`
- **Method:** `GET`
- **Description:** Retrieves details of a specific comment.
- **Headers:**
  - `Authorization: Bearer {token}`

##### Update Comment
- **URL:** `/api/v1/comments/{id}`
- **Method:** `PUT`
- **Description:** Updates a specific comment.
- **Headers:**
  - `Authorization: Bearer {token}`
- **Body Parameters:**
  - `content` (string, optional)

##### Delete Comment
- **URL:** `/api/v1/comments/{id}`
- **Method:** `DELETE`
- **Description:** Deletes a specific comment.
- **Headers:**
  - `Authorization: Bearer {token}`

##### Like/Unlike Comment
- **URL:** `/api/v1/comments/{id}/like`
- **Method:** `POST`
- **Description:** Likes or unlikes a comment.
- **Headers:**
  - `Authorization: Bearer {token}`